<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description"
	content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">

<title>Blog Curso Java Cloud</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/css/submit.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<nav>
				<ul class="nav nav-pills pull-right">
					<!-- <li role="presentation" class="active"><a href="#">Home</a></li>
					<li role="presentation"><a href="#">About</a></li>
					<li role="presentation"><a href="/autores">Autores</a></li>-->
					<c:choose>
						<c:when test="${not empty sessionScope.userLoggedIn}">
							<jsp:include page="includes/menu_logged.jsp" flush="true">
								<jsp:param name="submit" value="submit" />
								<jsp:param name="usuario" value="${sessionScope.userLoggedIn.nombre}" />
							</jsp:include>

						</c:when>
						
						<c:otherwise>
							<jsp:include page="includes/menu.jsp">
								<jsp:param name="submit" value="submit" />
							</jsp:include>
						</c:otherwise>
					</c:choose>
				</ul>
			</nav>
			<h3 class="text-muted">Blog Core</h3>

		</div>

		<div class="col-lg-8 col-lg-offset-2">

			<div class="text-center">
				<h3>Crear Post</h3>
				<form:form id="post-form" action="/submit/newpost" method="post" role="form"
					autocomplete="off" modelAttribute="crearPost">
					<div class="form-group">
						<form:input type="text" name="titulo" id="titulo" tabIndex="1"
							class="form-control" path="titulo" placeholder="Titulo" />
					</div>
					<div class="form-group">
						<form:input type="url" name="url" id="url" tabIndex="3"
							class="form-control" path="url" placeholder="Url (Opcional)" />
					</div>
					<div class="form-group">
						<form:textarea class="form-control textarea" rows="3" name="text"
							id="texto" placeholder="Contenido" path="contenido"></form:textarea>
					</div>
					

					<div class="form-group">
						<button type="submit" name="post-submit" id="post-submit"
							tabIndex="4" class="form-control btn btn-info">Enviar Post </button>

					</div>
				</form:form>
			</div>


		</div>



	</div>


	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/messages_es.js"></script>
	<script src="<c:url value="/assets/wysiwyg/bootstrap3-wysihtml5.all.min.js" />"></script>
	<script>
		$(document).ready(function() {
			$("#texto").wysihtml5();
		});
	
	</script>
</body>
</html>