package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Comment;

@Repository
@Transactional
public class CommentDao {
		
	    @PersistenceContext
		private EntityManager entityManager;
	    /*
	     * Almacena el Comment en la base de datos
	     */
	    public void create(Comment comment) {
	    	entityManager.persist(comment);
	    	return;
	    }
	    
	    @SuppressWarnings("unchecked")
		public List<Comment> getAll(){
	    	return entityManager
	    			.createQuery("select c from Comment c")
	    			.getResultList();
	    }
	    
	    
	 	/**
		 * Devuelve un post en base a su Id
		 */
		public Comment getById(long id) {
			return entityManager.find(Comment.class, id);
		}

	    
	    
	    
	}
