package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Post;
import com.corenetworks.hibernate.blog.model.User;

@Repository
@Transactional
public class PostDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el post en la base de datos
     */
    public void create(Post post) {
    	entityManager.persist(post);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Post> getAll(){
    	return entityManager
    			.createQuery("select p from Post p")
    			.getResultList();
    }
    
    
 	/**
	 * Devuelve un post en base a su Id
	 */
	public Post getById(long id) {
		return entityManager.find(Post.class, id);
	}

    
    
    
}