package com.corenetworks.hibernate.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.RegisterBean;
import com.corenetworks.hibernate.blog.dao.UserDao;
import com.corenetworks.hibernate.blog.model.User;

@Controller
public class RegisterController {
	@Autowired
	private UserDao userDao;

	@GetMapping(value = "/signup")
	public String showForm(Model model) {
		model.addAttribute("userRegister", new RegisterBean());
		return "register";
	}

	@PostMapping(value = "/register")
	public String submit(@ModelAttribute("userRegister") RegisterBean r, Model model) {
		userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));

		return "redirect:/autores";
	}
}
