package com.corenetworks.hibernate.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.blog.dao.PostDao;

@Controller
public class MainController {

	@Autowired
	private PostDao postDao;
	
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		
		modelo.addAttribute("listaPost", postDao.getAll());
		
		return "index";
		
	}
}
